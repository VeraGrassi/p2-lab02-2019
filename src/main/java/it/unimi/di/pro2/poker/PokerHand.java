package it.unimi.di.pro2.poker;

import ca.mcgill.cs.stg.solitaire.cards.Card;
import ca.mcgill.cs.stg.solitaire.cards.Deck;
import ca.mcgill.cs.stg.solitaire.cards.Rank;
import ca.mcgill.cs.stg.solitaire.cards.Suit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class PokerHand implements Iterable<Card>, Comparable<PokerHand>{
    ArrayList<Card> mano=new ArrayList<Card>();
    public PokerHand(Deck deck, int numCarte){
        for(int i=0; i<numCarte; i++){
            mano.add(deck.draw());
        }
    }

    @Override
    public Iterator<Card> iterator() {
        return mano.iterator();
    }

    public HandRank getRank(){
        ChainedHandEvaluator chain=new Tris(new Coppia(new CartaAlta()));
        return chain.evaluator(this);
    }

    @Override
    public int compareTo(PokerHand hand) {
        return this.getRank().compareTo(hand.getRank());
    }

    public String toString(){
        StringBuilder s=new StringBuilder("");
        for (Card card : mano) {
            s.append(card.toString()+"\n");
        }
        return s.toString();
    }
    public PokerHand(String manos){
        String stringa;
        char r;
        char s;

        Scanner scan=new Scanner(manos);
        while(scan.hasNext()){
            stringa=scan.next().trim();
            r=stringa.charAt(0);
            s=stringa.charAt(1);
            if(rango(r)!=null && suit(s)!=null){
                mano.add(Card.get(rango(r), suit(s)));
            }
        }
    }

    private Rank rango(char r){
        switch (r){
            case '0': return Rank.TEN;
            case '1': return Rank.ACE;
            case '2': return Rank.TWO;
            case '3': return Rank.THREE;
            case '4': return Rank.FOUR;
            case '5': return Rank.FIVE;
            case '6': return Rank.SIX;
            case '7': return Rank.SEVEN;
            case '8': return Rank.EIGHT;
            case '9': return Rank.NINE;
            case 'J': return Rank.JACK;
            case 'Q': return Rank.QUEEN;
            case 'K': return Rank.KING;
            default: return null;
        }
    }

    private Suit suit(char s) {
        switch (s) {
            case 'C':
                return Suit.CLUBS;
            case 'H':
                return Suit.HEARTS;
            case 'D':
                return Suit.DIAMONDS;
            case 'S':
                return Suit.SPADES;
            default:
                return null;
        }
    }
}
