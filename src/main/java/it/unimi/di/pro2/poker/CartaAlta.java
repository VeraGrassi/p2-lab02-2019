package it.unimi.di.pro2.poker;

public class CartaAlta implements ChainedHandEvaluator {

    @Override
    public HandRank evaluator(PokerHand hand) {
        return HandRank.HIGH_CARD;
    }
}
