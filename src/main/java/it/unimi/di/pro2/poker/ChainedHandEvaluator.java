package it.unimi.di.pro2.poker;

public interface ChainedHandEvaluator {
    HandRank evaluator(PokerHand hand);
}
