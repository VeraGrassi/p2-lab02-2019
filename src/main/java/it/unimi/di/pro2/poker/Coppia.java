package it.unimi.di.pro2.poker;

import ca.mcgill.cs.stg.solitaire.cards.Card;

public class Coppia implements ChainedHandEvaluator{
    ChainedHandEvaluator successore;
    public Coppia(ChainedHandEvaluator prossimo) {
        successore = prossimo;
    }
    @Override
    public HandRank evaluator(PokerHand hand) {
        int cont=0;
        for (Card c : hand) {
            for (Card card : hand) {
                if(c.getRank()==card.getRank()) {
                    cont++;
                }
            }
            if(cont==2){
                return HandRank.TWO_PAIR;

            }
        }
        return successore.evaluator(hand);
    }

}
