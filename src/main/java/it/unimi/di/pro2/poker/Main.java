package it.unimi.di.pro2.poker;

import ca.mcgill.cs.stg.solitaire.cards.Deck;

public class Main {
    public static void main(String[]args) {
        Deck deck = new Deck();
        PokerHand pokerHand = new PokerHand(deck, 5);
        System.out.println(pokerHand);
        System.out.println(pokerHand.getRank() + "\n");
        PokerHand pokerHand1 = new PokerHand("0H JC JH 1C 4C");
        System.out.println(pokerHand1); 
        System.out.println("bb"); 
    }
}
