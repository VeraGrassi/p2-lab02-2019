package it.unimi.di.pro2.poker;

import ca.mcgill.cs.stg.solitaire.cards.Card;

import java.util.ArrayList;

public class Tris implements ChainedHandEvaluator{
    ChainedHandEvaluator successore;
    public Tris(ChainedHandEvaluator prossimo){
        successore=prossimo;
    }

    @Override
    public HandRank evaluator(PokerHand hand) {
        int cont=0;
        for (Card c : hand) {
            for (Card card : hand) {
                if(c.getRank()==card.getRank()) {
                    cont++;
                }
            }
            if(cont==3){
                return HandRank.THREE_OF_A_KIND;

        }
        }
        return successore.evaluator(hand);
    }
}
